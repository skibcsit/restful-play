package controllers

import javax.inject.Inject
import play.api.mvc._
import play.api.routing.Router
import play.api.routing.Router.Routes
import views.html.defaultpages.badRequest

import scala.collection.mutable
import scala.collection.mutable.Map
import scala.runtime.AbstractPartialFunction


abstract class AbstractResource[T](implicit idBindable: PathBindable[T])
  extends Router.Routes with ResourceTrait[T] {


  private var path: String = ""
  private val MaybeSlash = "/?".r
  private val NewScreen = "/new/?".r
  private val Id = "/([^/]+)/?".r
  private val Edit = "/([^/]+)/edit/?".r

  def withId(id: String, action: String => EssentialAction) = action(id)

  def withPrefix(prefix: String) : Router = {
    new Router { def routes = routesDef

      override def documentation: scala.Seq[(_root_.scala.Predef.String, _root_.scala.Predef.String, _root_.scala.Predef.String)] = { throw new NotImplementedError()}

      override def withPrefix(prefix: String): Router = { throw new NotImplementedError()}
    }
  }

  def setPrefix(prefix: String) {
    path = prefix
  }

  def prefix = ""
  def documentation = Nil
  def subresourcesList = List[AbstractResource[Long]]()
//  def applyOrElse[A1 <: A, B1 >: B](x: A1, default: A1 => B1): B1 =
//  if (isDefinedAt(x)) apply(x) else default(x)

  def routesDef = new AbstractPartialFunction[RequestHeader, Handler] {
    override def applyOrElse[A <: RequestHeader, B >: Handler](rh: A, default: A => B) = defaultRouteApplication(rh, default)
    override def isDefinedAt(x: RequestHeader): Boolean = ???
  }

  def defaultRouteApplication[A <: RequestHeader, B >: Handler](rh: A, default: A => B, dictionary: Map[InjectedController, String] = null): B = {
    if(gotToTargetResource(rh.target.path)) {
      (rh.method, rh.target.uriString.drop(prefix.length + 2)) match {
        case ("GET", MaybeSlash()) => index(dictionary)
        case ("GET", NewScreen()) => newScreen(dictionary)
        case ("POST", MaybeSlash()) => create(dictionary)
        case _ => default(rh)
      }
    } else {
      def subresource = getSubresource(rh.target.path)
      if(subresource == null) default(rh)
      shortenPathAndAddValueToDictionary(dictionary, rh.target.path)
      subresource.defaultRouteApplication[A, B](rh: A, default: A => B, dictionary: Map[InjectedController, String])
    }
  }

  def gotToTargetResource(path: String) : Boolean = path.startsWith("/" + prefix)

  def suchSubresourceExists(path: String): Boolean = {
    def names = subresourcesList.map(x => x.toString())
    for (name <- names) {
      if (path.startsWith("/" + name)) return true
    }
    return false
  }

  def getSubresource(path: String) : AbstractResource[Long] = {
    if(suchSubresourceExists(path)) {
      subresourcesList.find(x => x.toString() == path.split('/')(0)).get
    }
    else return null
  }

  def shortenPathAndAddValueToDictionary(dictionary: Map[InjectedController, String], path: String) : String = {
    var shortened = path.substring(1).split('/')(0)
    //var arg = path.substring(1)
    dictionary.put(this, "")
    return shortened
  }



    override def isDefinedAt(x: RequestHeader): Boolean = ???


    def index = ???

  def index(args: mutable.Map[InjectedController, String]): EssentialAction = Action {Ok("abstract index")}

  def newScreen(args: Map[InjectedController, String]): EssentialAction = Action {Ok("abstract new screen")}

  def create(args: Map[InjectedController, String]): EssentialAction = Action {Ok("abstract create")}

  def show(args: Map[InjectedController, String], id: String): EssentialAction = Action {Ok("abstract show id " + id)}

  def edit(args: Map[InjectedController, String], id: String): EssentialAction = Action {Ok("abstract edit id " + id)}

  def update(args: Map[InjectedController, String], id: String): EssentialAction = Action {Ok("abstract update id " + id)}

  def destroy(args: Map[InjectedController, String], id: String): EssentialAction = Action {Ok("abstract destroy id " + id)}

  def defaultAction(url: String) = Action {NotFound("Can't find the requested page " + url)}

}
