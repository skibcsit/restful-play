package controllers

import javax.inject.Inject
import play.api.mvc.{AbstractController, ControllerComponents}


class ProjectsController @Inject()(cc: ControllerComponents) (implicit assetsFinder: AssetsFinder)
  extends AbstractController(cc) {

  def index = Action {
    Ok("Projects index.")
  }

  def newScreen = Action {
    Ok("Projects new screen.")
  }

  def create = Action {
    Ok("Projects create.")
  }

  def show(id: Int) = Action {
    Ok("Projects show id " + id.toString)
  }

  def edit(id: Int) = Action {
    Ok("Projects edit id " + id.toString)
  }

  def update(id: Int) = Action {
    Ok("Projects update id " + id.toString)
  }

  def destroy(id: Int) = Action {
    Ok("Projects destroy id " + id.toString)
  }
}