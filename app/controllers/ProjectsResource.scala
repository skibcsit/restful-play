package controllers
import javax.inject._
import play.api.mvc._
import play.api.routing.Router
import play.api.routing.Router.Routes

import scala.collection.mutable

@Singleton
class ProjectsResource @Inject() (controller: ProjectsController) extends AbstractResource[Long] {

  def subresources = List[AbstractResource[Long]]()

  override def index(args: Map[InjectedController, String]): EssentialAction = controller.index

  override def newScreen(args: Map[InjectedController, String]): EssentialAction = controller.newScreen

  override def create(args: Map[InjectedController, String]): EssentialAction = controller.create

  override def show(args: Map[InjectedController, String], id: String): EssentialAction    = ??? //= controller.show(HelpersClass.ExtractProjectsId(args))

  override def edit(args: Map[InjectedController, String], id: String): EssentialAction    = ??? //= controller.edit(HelpersClass.ExtractProjectsId(args))

  override def update(args: Map[InjectedController, String], id: String): EssentialAction  = ??? //= controller.update(HelpersClass.ExtractProjectsId(args))

  override def destroy(args: Map[InjectedController, String], id: String): EssentialAction = ??? //= controller.destroy(HelpersClass.ExtractProjectsId(args))

  override def defaultRouteApplication[A <: RequestHeader, B >: Handler](rh: A, default: A => B): Unit = ???

  override def apply(v1: RequestHeader): Handler = ???
}