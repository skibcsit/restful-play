package controllers

import play.api.mvc.{EssentialAction, Handler, InjectedController, RequestHeader}

trait ResourceTrait[T] extends InjectedController {
  def index(args: Map[InjectedController, String]): EssentialAction
  def newScreen(args: Map[InjectedController, String]): EssentialAction
  def create(args: Map[InjectedController, String]): EssentialAction
  def show(args: Map[InjectedController, String], id: String): EssentialAction
  def edit(args: Map[InjectedController, String], id: String): EssentialAction
  def update(args: Map[InjectedController, String], id: String): EssentialAction
  def destroy(args: Map[InjectedController, String], id: String): EssentialAction
  def defaultRouteApplication[A <: RequestHeader, B >: Handler](rh: A, default: A => B)
}
