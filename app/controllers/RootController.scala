package controllers

import javax.inject.Inject
import play.api.mvc.{AbstractController, ControllerComponents}

class RootController @Inject()(cc: ControllerComponents)(implicit assetsFinder: AssetsFinder)
  extends AbstractController(cc) {

  def index = Action {
    Ok("Root index.")
  }

}