package controllers

import javax.inject._
import play.api.mvc.{EssentialAction, Handler, InjectedController, RequestHeader}

import scala.collection.mutable

class RootResource @Inject()(controller: RootController, projectsResource: ProjectsResource, usersResource: UsersResource) extends AbstractResource[Long] {

  override def prefix = ""
  override def subresourcesList = List[AbstractResource[Long]](projectsResource, usersResource)



  override def index(args: Map[InjectedController, String]): EssentialAction = controller.index

  override def newScreen(args: Map[InjectedController, String]): EssentialAction = ???

  override def create(args: Map[InjectedController, String]): EssentialAction = ???

  override def show(args: Map[InjectedController, String], id: String): EssentialAction = ???

  override def edit(args: Map[InjectedController, String], id: String): EssentialAction = ???

  override def update(args: Map[InjectedController, String], id: String): EssentialAction = ???

  override def destroy(args: Map[InjectedController, String], id: String): EssentialAction = ???

  override def defaultRouteApplication[A <: RequestHeader, B >: Handler](rh: A, default: A => B): Unit = ???

  override def apply(v1: RequestHeader): Handler = ???
}