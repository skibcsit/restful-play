package controllers

import javax.inject.Inject
import play.api.mvc.{AbstractController, ControllerComponents, EssentialAction}

class UsersController @Inject()(cc: ControllerComponents) (implicit assetsFinder: AssetsFinder)
  extends AbstractController(cc) {


  def index = Action {
    Ok("Users index.")
  }

  def index(projectId: Int) = Action {
    Ok("Users index with project id " + projectId.toString)
  }

  def newScreen = Action {
    Ok("Users new screen.")
  }

  def create = Action {
    Ok("Users create.")
  }

  def show(id: Int) = Action {
    Ok("Users show id " + id.toString)
  }

  def show(projectId: Int, userId: Int) = Action {
    Ok("Users show id " + userId.toString + " with project id " + projectId.toString)
  }

  def edit(id: Int) = Action {
    Ok("Users edit id " + id.toString)
  }

  def update(id: Int) = Action {
    Ok("Users update id " + id.toString)
  }

  def destroy(id: Int) = Action {
    Ok("Users destroy id " + id.toString)
  }
}