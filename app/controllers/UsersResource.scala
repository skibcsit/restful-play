package controllers
import javax.inject.Inject
import play.api.mvc.{EssentialAction, Handler, InjectedController, RequestHeader}

import scala.collection.mutable

class UsersResource @Inject() (controller: UsersController) extends AbstractResource[Long] {

  override def prefix = "users" //TODO: сделать зависимым от имени класса?

  override def index(args: Map[InjectedController, String]): EssentialAction = controller.index

  override def newScreen(args: Map[InjectedController, String]): EssentialAction = controller.newScreen

  override def create(args: Map[InjectedController, String]): EssentialAction = controller.create

  override def show(args: Map[InjectedController, String], id: String): EssentialAction = ???
//    {
//      (args.contains(ProjectsResource), args.contains(this)) match
//      {
//        case (true, true) => controller.show(HelpersClass.ExtractProjectsId(args), HelpersClass.ExtractUsersId(args))
//        case (true, false) => controller.index(HelpersClass.ExtractProjectsId(args));
//        case _ => defaultAction(id);
//      }
//      controller.show(HelpersClass.ExtractProjectsId(args))
//    }

  override def edit(args: Map[InjectedController, String], id: String): EssentialAction = ??? //controller.edit(HelpersClass.ExtractUsersId(args))

  override def update(args: Map[InjectedController, String], id: String): EssentialAction = ??? //controller.update(HelpersClass.ExtractUsersId(args))

  override def destroy(args: Map[InjectedController, String], id: String): EssentialAction = ??? //controller.destroy(HelpersClass.ExtractUsersId(args))

  override def defaultRouteApplication[A <: RequestHeader, B >: Handler](rh: A, default: A => B): Unit = ???

  override def apply(v1: RequestHeader): Handler = ???
}
