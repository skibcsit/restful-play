import org.scalatestplus.play._
import org.scalatestplus.play.guice.GuiceOneServerPerTest

/**
 * Runs a browser test using Fluentium against a play application on a server port.
 */
class BrowserSpec extends PlaySpec
  with OneBrowserPerTest
  with GuiceOneServerPerTest
  with HtmlUnitFactory
  with ServerProvider {

  "Application" should {

    "work from within a browser" in {

      go to ("http://localhost:" + port)

      pageSource must include ("Root index.")
    }
  }

  "Application" should {

    "work from within a browser" in {

      go to ("http://localhost:" + port + "/projects")

      pageSource must include ("Projects index.")
    }
  }

  "Application" should {

    "work from within a browser" in {

      go to ("http://localhost:" + port + "/users")

      pageSource must include ("Users index.")
    }
  }

}
