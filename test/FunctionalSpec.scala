import org.scalatestplus.play.PlaySpec
import org.scalatestplus.play.guice.GuiceOneAppPerSuite
import play.api.http.Status
import play.api.test.FakeRequest
import play.api.test.Helpers._

/**
 * Functional tests start a Play application internally, available
 * as `app`.
 */
class FunctionalSpec extends PlaySpec with GuiceOneAppPerSuite {

  "Routes" should {

    "send 404 on a bad request" in  {
      route(app, FakeRequest(GET, "/nonexistantpage")).map(status(_)) mustBe Some(NOT_FOUND)
    }

    "send 200 on a good request" in  {
      route(app, FakeRequest(GET, "/")).map(status(_)) mustBe Some(OK)
    }

  }

  "ProjectsController" should {

    "render the index page" in {
      val home = route(app, FakeRequest(GET, "/projects")).get

      status(home) mustBe Status.OK
      contentType(home) mustBe Some("text/html")
      contentAsString(home) must include ("Projects index")
    }

  }

  "ProjectsController" should {

    "render the new screen page" in {
      val home = route(app, FakeRequest(GET, "/projects/new")).get

      status(home) mustBe Status.OK
      contentType(home) mustBe Some("text/html")
      contentAsString(home) must include ("Projects new screen")
    }

  }

  "UsersController" should {

    "render the index page" in {
      val home = route(app, FakeRequest(GET, "/users")).get

      status(home) mustBe Status.OK
      contentType(home) mustBe Some("text/html")
      contentAsString(home) must include ("Users index")
    }

  }

  "UsersController" should {

    "render the edit page" in {
      val home = route(app, FakeRequest(GET, "/users/3/edit")).get

      status(home) mustBe Status.OK
      contentType(home) mustBe Some("text/html")
      contentAsString(home) must include ("Projects edit id 3")
    }

  }

  "UsersController" should {

    "render the edit page within project" in {
      val home = route(app, FakeRequest(GET, "/projects/2/users/3/edit")).get

      status(home) mustBe Status.OK
      contentType(home) mustBe Some("text/html")
      contentAsString(home) must include ("Projects edit id 3 in project id 2")
    }

  }

}
